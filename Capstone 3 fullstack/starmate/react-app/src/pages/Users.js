import Table from 'react-bootstrap/Table';
import { useEffect, useState } from 'react';
import UserCard from '../components/UserCard';


export default function Users(){
	  // State that will be used to store the products retrieved from the database
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState([]);
  const [updateCounter, setUpdateCounter] = useState(0);
  console.log("puta")
  // Retrieves the products from the database upon initial render of the component
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // Sets the "users" state to map the data retrieved from the fetch request into several "UserCard" components
            setUsers(data);

        setUser(data.map(user => {
          return (
            <UserCard key={user._id} UserProp = {user}/>
            )
      }))
    })
  }, [updateCounter])


  const handleUpdateUser = () => {
    // update product logic here
    setUpdateCounter(updateCounter + 1); // increment updateCounter to trigger a rerender
    console.log(updateCounter)
  }

  return (
    <>
      <h3>Users</h3>
      <div className="table-responsive-mobile" >
      <Table striped bordered hover variant="light-blue">
        <thead>
          <tr>
            <th style={{ width: '20%' }}>Name</th>
            <th style={{ width: '15%' }}>Mobile Number</th>
            <th style={{ width: '5%' }}>Email</th>            
            <th style={{ width: '10%' }}>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map(user=> (
            <UserCard key={user._id} user={user} onUpdate={handleUpdateUser} />
          ))}
        </tbody>
      </Table>
      </div>
    </>
  );


}
