import { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import CartCard from '../components/CartCard';
import { Card, Button } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function Cart({userId, cartItems}) {
  // State that will be used to store the products retrieved from the database
  const [product, setProduct] = useState([]);
  const [products, setProducts] = useState([]);
  const [updateCounter, setUpdateCounter] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  const [isAdmin, setIsAdmin] = useState(false);
  const navigate = useNavigate();

  // Retrieves the products from the database upon initial render of the component
useEffect(() => {
  console.log('useEffect hook executed');
  const userId = localStorage.getItem('userId');
  const token = localStorage.getItem('token');

  fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then(response => response.json())
    .then(data => {
      console.log(data)
      const incompleteOrders = data.data.filter(order => order.status === "Order has not yet been completed");
      const productData = incompleteOrders.map(order => {
        return order.orderDetail.products.map(product => {
          return {
            productName: product.productName,
            productId: product.productId,
            imageUrl: product.imageUrl,
            quantity: product.quantity,
            pricePerItem: product.pricePerItem,
            totalPrice: product.totalPrice
          };
        });
      });
      setProducts(productData.flat());
      const total = productData.flat().reduce((sum, product) => sum + product.totalPrice, 0);
      console.log('Total:', total);
      setTotalAmount(total);
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}, [updateCounter]);


  const handleUpdateProduct = () => {
    // update product logic here
    setUpdateCounter(updateCounter + 1); // increment updateCounter to trigger a rerender
    console.log(updateCounter)
  }

  const checkOut = () => {
  fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify({
      userId: userId,
      isAdmin: isAdmin
    })
  })
  .then(res => {
    if (!res.ok) {
      throw new Error('Network response was not ok');
    }
    return res.text();
  })
  .then(data => {
    Swal.fire({
      title: 'Thank you for your purchase!',
      icon: 'success'
    });

  })
  .catch(error => {
    console.error('Error:', error);
    Swal.fire({
      title: 'Oops!',
      text: 'Something went wrong. Please try again later.',
      icon: 'error'
    });
  });
}


  const handleCheckout = () => {
    Swal.fire({
        title: 'Are you sure you want to complete this order?',
        text: 'Do you want to proceed with checkout?',
        showCancelButton: true,
        cancelButtonText: 'No',
        cancelButtonColor: '#d33', // red color
        confirmButtonText: 'Yes',
        confirmButtonColor: '#3085d6', // blue color
        reverseButtons: false
    }).then((result) => {
      if (result.isConfirmed) {
        console.log('User clicked the "Yes, checkout!" button');
        checkOut()
        navigate('/');
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // User clicked the "No, cancel!" button, so you can do something else here
        console.log('User clicked the "No, cancel!" button');
      }
    });
  };

  return (
    <>
      {products.length > 0 ? (
        <>
        <h1>My Cart</h1>
        <Table striped bordered hover variant="light-blue" >
          <thead>
            <tr>
              <th style={{ width: '40%', textAlign: 'center' }}>Product</th>
              <th style={{ width: '25%', textAlign: 'center' }}>Quantity</th>
              <th style={{ width: '10%', textAlign: 'center' }}>Total</th>
              <th style={{ width: '25%', textAlign: 'center' }}>Action</th>
            </tr>
          </thead>
          <tbody >
            {products.map(product => (
              <CartCard key={product.productName} productProp={product} updateCart={handleUpdateProduct} />
            ))}
          </tbody>
        </Table>
        <br></br>
        <br></br>
        <div className="row align-items-center px-md-5">
          <div className="col-md-6 text-md-start text-center">
            <h3 className="d-inline-block">Your total amount is {totalAmount.toLocaleString('en-PH', {style: 'currency', currency: 'PHP'})}</h3>
          </div>
          <div className="col-md-6 text-md-end text-center">
            <Button onClick={handleCheckout} className="w-50" style={{ height: '50px' }}>Checkout</Button>
          </div>
        </div>
        </>
        ) : (
        <>
        <div className="d-flex flex-column align-items-center text-center">
          <h1>Your cart is Empty</h1>
          <Button as={Link} to="/products">Products</Button>
        </div>
        </>
        )
    }
    </>
  );
}

