import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';


export default function Products() {

	// State that will be used to store the products retrieved from the database
	const [ product, setProducts ] = useState([]);

	// Retrieves the products from the database upon intial render of the "Products" component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" components
			setProducts(data.map(courses => {
				return (
					<div key={courses._id} className="col-lg-3 col-md-4 col-sm-12 mb-4 product-card animate__animated animate__fadeIn">
						<ProductCard productProp={courses} />
					</div>
				)
			}))
		})
	}, [])

	return (
		<>
			<h1>Products</h1>
			<br></br>
			<div className="row">
				{courses}
			</div>
		</>
	)
}	
