import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';




export default function Register(){

    // State hooks tto store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState('');

    const {user} = useContext(UserContext);

    //an object with methods to redirect the user
    const navigate = useNavigate();


    console.log(email);
    console.log(password1);
    console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {

            // Prevents page redirection via form submission
            e.preventDefault();

            fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email
                })
            })
            .then(res => res.json())
            .then(data => {

                console.log(data);

                if(data === true){

                    Swal.fire({
                        title: 'Duplicate email found',
                        icon: 'error',
                        text: 'Please provide a different email.'   
                    });

                } else {

                    fetch(`${ process.env.REACT_APP_API_URL }/users/userRegistration`, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            mobileNo: mobileNo,
                            password: password1
                        })
                    })
                    .then(res => res.json())
                    .then(data => {

                        console.log(data);

                        if(data === true){

                            // Clear input fields
                            setFirstName('');
                            setLastName('');
                            setEmail('');
                            setMobileNo('');
                            setPassword1('');
                            setPassword2('');

                            Swal.fire({
                                title: 'Registration successful',
                                icon: 'success',
                                text: 'Welcome to Zuitt!'
                            });

                            // Allows us to redirect the user to the login page after registering for an account
                            navigate("/login");

                        } else {

                            Swal.fire({
                                title: 'Something wrong',
                                icon: 'error',
                                text: 'Please try again.'   
                            });

                        };

                    })
                };

            })

        }



useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [firstName, lastName, email, mobileNo, password1, password2]);
  // ----------------------------------------------------------------------------

  return(
    
    (user.id !== null) ?
      <Navigate to="/products" />

     : 

    <div style={{ display: "flex", justifyContent: "center", height: "80vh" }}> 
      <Form
        onSubmit={(e) => registerUser(e)}
        style={{
          width: "400px",
          border: "1px solid grey",
          borderRadius: "10px",
          padding: "20px"
        }}
      >
        <h3>Register Here</h3>
        <br></br>
        <Form.Group controlId="userFirstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter First Name"
            required
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </Form.Group>
        <br></br>
        <Form.Group controlId="userLastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Last Name"
            required
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </Form.Group>
        <br></br>
        <Form.Group controlId="userMobileNumber">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control
            type="tel"
            placeholder="Enter Mobile Number"
            required
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
          />
          <Form.Text className="text-muted">
            Please enter a mobile number with at least 11 digits.
          </Form.Text>
        </Form.Group>
        <br></br>
        <Form.Group controlId="userEmail">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter email"
            required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>
        <br></br>
        <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            required
            value={password1}
            onChange={(e) => setPassword1(e.target.value)}
          />
        </Form.Group>
        <br></br>
        <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Verify password"
            required
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
          />
        </Form.Group>
        <br></br>
        <br></br>
        {/*conditional render submit button based on isActive state*/}

        {isActive ? 
          <Button
            variant="success"
            type="submit"
            id="submitBtn"
            style={{ width: "100%" }}
          >
            Submit
          </Button>
        : 
          <Button
            variant="primary"
            type="submit"
            id="submitBtn"
            style={{ width: "100%" }}
            disabled
          >
            Submit
          </Button>
        }
        
      </Form>
    </div>
     
   
  );
}
