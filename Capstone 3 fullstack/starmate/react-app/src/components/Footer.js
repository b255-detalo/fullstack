import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';

export default function Footer() {
  return (
    <footer>
      <div className="social-media-icons text-center">
        <a href="https://www.facebook.com/detalo.apollo/"><FontAwesomeIcon icon={faFacebook} size="2x" className="mr-3 text-black" /></a>
        <a href="https://twitter.com/ApolloTwelv"><FontAwesomeIcon icon={faTwitter} size="2x" className="mr-3 text-black" /></a>
        <a href="https://www.instagram.com/"><FontAwesomeIcon icon={faInstagram} size="2x" className="text-black" /></a>
      </div>
      <p className="text-center mt-3">&copy; All Rights Reserved | Starmate Store | 2023</p>
    </footer>
  );
}

