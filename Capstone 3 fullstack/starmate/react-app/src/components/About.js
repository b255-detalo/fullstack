import { Container } from 'react-bootstrap';

export default function About() {
  return (
    <Container fluid className ="vh-50" style={{ 
      color: 'black',
      padding: '50px',
      fontFamily: 'sans-serif',
      fontSize: '1.2rem'
    }}>
      <div>
        <h1 style={{color: '#76B900'}}>Starmate Store</h1> 
        <br></br>
        <p style={{textAlign: 'justify'}}> Unleash the full potential of your gaming rig with Starmate
              Store's selection of high-end gaming GPUs. Our top-of-the-line
              graphics cards deliver unparalleled performance, ensuring smooth
              gameplay and stunning visuals. With brands like Nvidia and AMD,
              you can trust that you're getting the best of the best. Shop now
              and elevate your gaming experience to the next level..</p>
        <p style={{textAlign: 'justify'}}>Serious gamers demand serious hardware, and that's exactly what
              you'll find at Starmate Store. Our collection of high-end gaming
              GPUs features cutting-edge technology and state-of-the-art design,
              ensuring that you're always ahead of the competition. Plus, with
              our competitive pricing and unbeatable customer service, you can
              shop with confidence. Take your gaming to the next level with
              Starmate Store..</p>
        <p style={{textAlign: 'justify'}}>At Starmate Store, we believe that every gamer deserves the best.
              That's why we offer a wide selection of high-end gaming GPUs to
              suit any budget and gaming style. Our knowledgeable team is here
              to help you find the perfect graphics card for your needs, so you
              can enjoy unparalleled performance and jaw-dropping graphics. Shop
              with us today and experience gaming like never before..</p>
        <br></br>
        
      </div>
    </Container>
  );
}
