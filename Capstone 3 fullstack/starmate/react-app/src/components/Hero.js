import React from 'react';
import { Link } from 'react-router-dom';
import './Hero.css';
import pic from "../green.jpg"

export default function HeroImage() {
  return (
    <header style={{ paddingLeft: 0 }}>
      <div
        className='p-5 text-center bg-image'
        style={{ 
          backgroundImage: `url(${pic})`, 
          height: 350,
          position: 'relative' // add position: 'relative' to enable z-index
/*          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover'*/
        }}
      >
        <div 
          className='mask' 
          >
        </div>
        <div 
          className='d-flex justify-content-center align-items-center h-100'
          style={{ position: 'relative', zIndex: 1 }}> {/* add position: 'relative' and zIndex: 1 to enable stacking context and control the text glow effect */}
          <div className='text-white'>
            <h1 className='mb-3 pt-5' >Welcome to Starmate Store</h1> {/* add textShadow CSS property to enable the text glow effect */}
            <h4 className='mb-3 pb-5' >"Just quality products"</h4>

            <Link to='/products'>
              <button className='btn btn-outline-light btn-lg btn-pulse'>Products</button>
            </Link>
          </div>
        </div>
      </div>
    </header>
  );
}

