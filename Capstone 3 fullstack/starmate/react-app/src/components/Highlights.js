import { useEffect, useState } from 'react';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { Card, Button } from 'react-bootstrap';
import './Hero.css';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Container, Row, Col } from 'react-bootstrap';
import './Highlights.css';
import picLaptop from "../msi gpu.jpg"
import picCellphone from "../gpu3.jpg"

export default function Highlights() {
  return (
    <Container>
      <h3 className="mb-4 text-left" style={{ color: '#76B900' }}>Only the best deals!</h3>
      <Row>
        <Col xs={12} md={6} className="pb-3 pb-md-0">
          <div className="image-container">
            <img src={picLaptop} className="img-fluid" alt="First image" style={{ maxHeight: 900 }} />
            <div className="button-container">
              <Link to={`/products/${'64183761c82c1a115280717a'}`} className='btn btn-outline-light btn-lg btn-pulse'>MSI Gaming GeForce GTX 1660</Link>
            </div>
          </div>
        </Col>
        <Col xs={12} md={6} className="pb-3 pb-md-0">
          <div className="image-container">
            <img src={picCellphone} className="img-fluid" alt="Second image" style={{ maxHeight: 900 }} />
            <div className="button-container">
              <Link to={`/products/${'64182d16c82c1a115280714a'}`} className='btn btn-outline-light btn-lg btn-pulse'>Gigabyte GeForce RTX 3060 Ti</Link>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
