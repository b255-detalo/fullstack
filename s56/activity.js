// Check first whether the letter is a single character.
// If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
// If letter is invalid, return undefined.

function countLetter(letter, sentence) {
    // Checks if the letter is a single character
    if (letter.length !== 1) {
        return undefined;
    } 

    let count = 0;

    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        count++;
      }
    }
  
    return count;
     
   }
   



// -----------------------------------------

// An isogram is a word where there are no repeating letters.
// The function should disregard text casing before doing anything else.
// If the function finds a repeating letter, return false. Otherwise, return true.

function isIsogram(text) {
    //if empty return true.
    if (text.isEmpty) {
      return true;
    } else {
      // All lower case.
      text = text.toLowerCase();
    }
    //split string into individual characters. 
    var array = text.split('');
    var sortedArr = array.slice().sort();
  
    for (var i = 0; i < array.length; i++) {
      //if duplicate is found return false.
      if (sortedArr[i + 1] == sortedArr[i]) {
        return false;
      }
    }
    //else return true
    return true;
  }
  
  
  //testing
  
  isIsogram('to be or not to be'); //should return false
  isIsogram('wasdfgh'); //should return true
  isIsogram(); //should return true
  





// ----------------------------------------------------------------

// Return undefined for people aged below 13.
// Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
// Return the rounded off price for people aged 22 to 64.
// The returned value should be a string.
    
function purchase(age, price) {
      if (age < 13) {
        return undefined;
      } else if (age >= 13 && age <= 21 || age >= 65) {
            const discountedPrice = Math.round(0.8 * price); // Discount formula will be applied
            return `Discounted price: ${discountedPrice}`;
      } else {
            const roundedPrice = Math.round(price);
            return `Rounded price: ${roundedPrice}`;
      }
}
// discount = Math.round(discount); // will do round off for exact discount





// -------------------------------------------

// Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const items = [
      { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
      { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
      { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
      { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
      { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

      ];

    function findHotCategories(items) {
          let newArr = [];    
          let finder = items.length

          for(let i = 0; i < finder; i++){
              if(items[i].stocks === 0){
              newArr.push(items[i].category)
            }
    }
          return [...new Set(newArr)];

    }
    console.log(findHotCategories(items));




// ------------------------------------------------------
// Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

function findFlyingVoters(candidateA, candidateB) {

    const tempVoter = candidateA.filter((data) => candidateB.includes(data));
    return tempVoter;
    
}

    console.log(findFlyingVoters(
      candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'],//A
      candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']//B
    ));











// --------------------------

 module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
}; 

