import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'

export default function Logout () {
    // Consume the UserContext object and destructure it to access the user state and unsetUser funtion from the context provider
    const {unsetUser, setUser} =  useContext(UserContext);

    // Clear the localStorage of the user's information
    unsetUser();

    // Placing the "setUser" setter function inside of a useEffect is necesssary because of updates within React JS that a state of another component be updated while trying to render a diferrent component
    // By adding the useEffect, this will allow the Logout page to render first before 

    useEffect(() => {
        // set the user state back to it's original value
        setUser({id: null});
    })


    return(
        <Navigate to='/login' />
    )
}